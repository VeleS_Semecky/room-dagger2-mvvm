package com.example.yurii.roomdagger2mvvm;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;


import com.example.roomdagger2mvvm2.ApplicationViewModel;
import com.example.roomdagger2mvvm2.dao.core.ICoreDAO;
import com.example.roomdagger2mvvm2.model.Name;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity implements ICoreDAO.InsertAll{
    Disposable d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView webview = findViewById(R.id.webview);
        webview.setVerticalScrollBarEnabled(true);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl("file:///android_asset/license_politics.html");
//        TextView textView = findViewById(R.id.textView);
//        textView.setText(getResources().getText(R.string.politics));

//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//
        ApplicationViewModel mRoomViewModel = ViewModelProviders.of(this).get(ApplicationViewModel.class);
        List<Name> names = new ArrayList<>();
        names.add(new Name(51, "1211121"));
        names.add(new Name(11, "1231121"));
        names.add(new Name(21, "1241121"));
        names.add(new Name(101, "12411231"));
        names.add(new Name(111, "124112311"));

        mRoomViewModel.getRepository().onInsertAll(names,this);

        mRoomViewModel.getRepository().getDao().getAllNames().observeForever(new Observer<List<Name>>() {
            @Override
            public void onChanged(@Nullable List<Name> names) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    assert names != null;
                    names.forEach(name -> Log.i("TAG_NAME", name.getId() + " " + name.getThName()));
                }
            }
        });
    }


    @Override
    public void insertAll(List<Long> count) {
        Log.i("TAG_LONG12",count.size()+"");
        for (long i:
                count) {
            Log.i("TAG_LONG1", i + " "+count.size());
        }

    }
}
