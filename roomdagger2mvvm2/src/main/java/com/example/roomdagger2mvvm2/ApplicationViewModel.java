package com.example.roomdagger2mvvm2;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;


import com.example.roomdagger2mvvm2.constant.RoomConstant;
import com.example.roomdagger2mvvm2.core.dagger.AppModule;
import com.example.roomdagger2mvvm2.core.dagger.DaggerComponentDI;
import com.example.roomdagger2mvvm2.core.dagger.ModuleRoom;

import javax.inject.Inject;

public class ApplicationViewModel extends AndroidViewModel {
    @Inject
    protected Repository repository;

    public Repository getRepository() {
        return repository;
    }

    public ApplicationViewModel(Application application) {
        super(application);
        DaggerComponentDI.builder()
                .appModule(new AppModule(application))
                .moduleRoom(new ModuleRoom(application, RoomConstant.NAME_ROOM_DATABASE))
                .build()
                .inject(this);
    }


    @Override
    protected void onCleared() {
        repository.unsubscribeRxJava();
        super.onCleared();
    }
}
