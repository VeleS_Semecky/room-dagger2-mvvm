package com.example.roomdagger2mvvm2.core;

import com.example.roomdagger2mvvm2.dao.core.CoreDAO;
import com.example.roomdagger2mvvm2.dao.core.ICoreDAO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseRepository<T> {
    private Disposable d;

    private CoreDAO<T> mNameDao;

    public BaseRepository(CoreDAO<T> nameDAO){
        this.mNameDao = nameDAO;
    }


    public Observable getCountInsertAll(List<T> list){
        return Observable.fromCallable(() -> mNameDao.insertAll(list)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    public void onInsertAll(List<T> list, ICoreDAO.InsertAll insertAll){
        d = Observable.fromCallable(() -> mNameDao.insertAll(list)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(longs -> {
                    insertAll.insertAll(longs);
                    d.dispose();
                });
    }

    public void onInsertOne(T t, ICoreDAO.InsertOne insertOne){
        d = Observable.fromCallable(() -> mNameDao.insertOne(t)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(longs -> {
                    insertOne.insertOne(longs);
                    d.dispose();
                });
    }

    public Observable getCountInsert(T name){
        return Observable.fromCallable(() -> mNameDao.insertOne(name))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public void unsubscribeRxJava() {
        if(!d.isDisposed())d.dispose();
    }
}
