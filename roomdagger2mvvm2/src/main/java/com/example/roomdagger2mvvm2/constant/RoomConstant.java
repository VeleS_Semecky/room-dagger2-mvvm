package com.example.roomdagger2mvvm2.constant;

public interface RoomConstant {

    String NAME_ROOM_DATABASE = "demo-db";

    int VERSION_ROOM_DATABASE = 1;

    interface TableNameRoomDatabase {
       String NAME = "name_table";
    }
}
