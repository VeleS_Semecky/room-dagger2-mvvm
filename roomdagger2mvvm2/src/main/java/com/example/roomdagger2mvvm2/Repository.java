package com.example.roomdagger2mvvm2;


import com.example.roomdagger2mvvm2.core.BaseRepository;
import com.example.roomdagger2mvvm2.model.Name;
import com.example.roomdagger2mvvm2.dao.NameDAO;


import javax.inject.Inject;
//TODO:Change DAO and TABLE
public class Repository extends BaseRepository<Name> {

    public NameDAO getDao() {
        return mNameDao;
    }

    private NameDAO mNameDao;

    @Inject
    public Repository(NameDAO nameDAO){
        super(nameDAO);
        this.mNameDao = nameDAO;
    }

}
