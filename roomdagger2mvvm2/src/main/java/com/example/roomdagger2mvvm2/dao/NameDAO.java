package com.example.roomdagger2mvvm2.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;


import com.example.roomdagger2mvvm2.constant.RoomConstant;
import com.example.roomdagger2mvvm2.dao.core.CoreDAO;
import com.example.roomdagger2mvvm2.model.Name;

import java.util.List;

@Dao
public interface NameDAO extends CoreDAO<Name> {
    //TODO:ADD Your QUERY and Name TABLE
    @Query("SELECT * FROM " + RoomConstant.TableNameRoomDatabase.NAME)
    LiveData<List<Name>> getAllNames();

    @Query("SELECT * FROM " + RoomConstant.TableNameRoomDatabase.NAME + " WHERE id = :thID")
    LiveData<Name> getSearchById(String thID);


    @Query("SELECT * FROM " + RoomConstant.TableNameRoomDatabase.NAME + " WHERE name =:thNAme")
    LiveData<Name> getSearchByName(String thNAme);


}
